package com.sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator
{
    private EmailValidator(){}

    public static boolean isValidEmail(String email)
    {
        Pattern pattern = Pattern.compile("^[a-z][a-z|0-9][a-z|0-9]+@[a-z|0-9][a-z|0-9][a-z|0-9].[a-z][a-z]+(.[a-z][a-z]+)?$");
        Matcher matcher = pattern.matcher(email);

        return matcher.find();
    }
}
