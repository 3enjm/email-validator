package com.sheridan.test;

import com.sheridan.EmailValidator;
import org.junit.Assert;
import org.junit.Test;

public class EmailValidatorTest {

    /*
     * Email must be in this format: <account>@<domain>.<TLD>
     */
    @Test
    public void isValidEmail_format_Exception() {
        Assert.assertFalse(EmailValidator.isValidEmail("com.sheridancollege@aholab"));
    }

    @Test
    public void isValidEmail_format_Regular() {
        Assert.assertTrue(EmailValidator.isValidEmail("aholab@sheridancollge.ca"));
    }

    /*
     * Email should have one and only one @ symbol
     */
    @Test
    public void isValidEmail_atSymbol_Exception() {
        Assert.assertFalse(EmailValidator.isValidEmail("foo@@bar.com"));
    }

    @Test
    public void isValidEmail_atSymbol_Regular() {
        Assert.assertTrue(EmailValidator.isValidEmail("foo@bar.com"));
    }

    /*
     * Account name should have atleast 3 alpha-characters in lowercase
     */
    @Test
    public void isValidEmail_localpart_BoundryIn() {
        Assert.assertTrue(EmailValidator.isValidEmail("foo@bar.com"));
    }

    @Test
    public void isValidEmail_localpart_BoundryOut() {
        Assert.assertFalse(EmailValidator.isValidEmail("fo@bar.com"));
    }

    @Test
    public void isValidEmail_localpart_Exception() {
        Assert.assertFalse(EmailValidator.isValidEmail("f@bar.com"));

    }

    @Test
    public void isValidEmail_localpart_Regular() {
        Assert.assertTrue(EmailValidator.isValidEmail("foobar@baz.com"));
    }

    /*
     * domain should have atleast 3 alpha-characters in lowercase or numbers
     */
    @Test
    public void isValidEmail_domain_BoundryIn() {
        Assert.assertTrue(EmailValidator.isValidEmail("foobar@baz.com"));
    }

    @Test
    public void isValidEmail_domain_BoundryOut() {
        Assert.assertFalse(EmailValidator.isValidEmail("foobar@ba.com"));
    }

    @Test
    public void isValidEmail_domain_Exception() {
        Assert.assertFalse(EmailValidator.isValidEmail("foobar@b.com"));
    }

    @Test
    public void isValidEmail_domain_Regular() {
        Assert.assertTrue(EmailValidator.isValidEmail("aholab@sheridancollege.com"));
    }

    /*
     * tld should have atleast 3 alpha-characters in lowercase
     */
    @Test
    public void isValidEmail_tld_BoundryIn() {
        Assert.assertTrue(EmailValidator.isValidEmail("aholab@sheridancollege.com"));
    }

    @Test
    public void isValidEmail_tld_BoundryOut() {
        Assert.assertFalse(EmailValidator.isValidEmail("aholab@sheridancollege.c"));
    }

    @Test
    public void isValidEmail_tld_Exception() {
        Assert.assertFalse(EmailValidator.isValidEmail("aholab@sheridancollege.c0"));
    }

    @Test
    public void isValidEmail_tld_Regular() {
        Assert.assertTrue(EmailValidator.isValidEmail("aholab@sheridancollege.com"));
    }

}
